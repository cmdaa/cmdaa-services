const { DataStore, MongoDataStore,
        Model,     Collection }     = require('..');
const { MockMongo,
        MockMongoCollection }       = require('./mock-mongo');

const mongo = new MockMongo();
const mds   = new MongoDataStore( mongo, Collection );
console.log('mds:', mds);

const c   = new Collection( mds );
console.log('c  :', c);

const m   = c.makeModel( {a:1, b:2, c:{d:3}} );
console.log('m  :', m);

console.log('m._id       :', m._id);
console.log('m.toJSON()  :', m.toJSON() );
console.log("m.get('a')  :", m.get('a') );
console.log("m.get('c.d'):", m.get('c.d') );
console.log("m.set('c.d'):", m.set('c.d', 42) );
console.log("m.get('c.d'):", m.get('c.d') );

m.save()
  .then( model => {
    console.log('>>> Save:', model.toJSON());
  })
  .catch( err => {
    console.log('*** Save:', err);
  });
