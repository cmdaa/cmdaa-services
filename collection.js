const { DataStore }           = require('./datastore');
const { isObject, deepMerge } = require('./deepMerge');
const { getPath, setPath }    = require('./objPath');

/**
 *  The abstract base-class for a model that exists as part of a collection
 *
 *  @class  Model
 */
class Model {
  /**
   *  The (lower-case) name of this model.
   *  
   *  @property $name {String};
   *  @static
   *  @readonly
   */
  static get $name()    { return this.name.toLowerCase() }

  /**
   *  The schema that defines valid model data.
   *
   *  @property $schema {Object};
   *  @static
   *  @readonly
   */
  static $schema  = {
    bsonType: 'object',
    required: ['_id'],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of this log',
      },
    },
  };

  // Proxies for static properties and methods
  get $schema()   { return this.constructor.$schema }
  get $name()     { return this.constructor.$name }

  /**
   *  Create a new model instance.
   *
   *  @constructor
   *  @param  data        The base data for this model instance {Object};
   *  @param  collection  The containing collection {Collection};
   */
  constructor( data, collection ) {
    Object.defineProperties( this, {
      /**
       *  The schema for this model.
       *
       *  @property $collection {Collection};
       *  @readonly
       */
      $schema     : { value: this.constructor.$schema },

      /**
       *  A reference to the containing collection.
       *
       *  @property $collection {Collection};
       *  @readonly
       */
      $collection : { value: collection },

      /**
       *  An alias/short-cut to $collection.$ds.
       *
       *  @property $ds {DataStore};
       *  @readonly
       */
      $ds : { value: collection.$ds },

      /**
       *  The data backing this model.
       *
       *  @property $data {Object};
       *  @readonly
       */
      $data : { value: {...data} },
    });
  }

  /**
   *  Get/Set the unique id of this model.
   *
   *  @property _id {String};
   *
   *  @note The _id may only be set once.
   */
  get _id()     { return this.$data._id }
  set _id( id ) { if (this._id == null) { this.$data._id = id } }

  /**
   *  Is this model backed by the underlying datastore?
   *
   *  @method $isBacked
   *
   *  @return true | false {Boolean};
   */
  $isBacked() { return this._id != null }

  /**
   *  Retrieve the value of the given property.
   *
   *  @method get
   *  @param  property  The property name or dotted-property path {String};
   *
   *  @return The property value {Mixed};
   */
  get( property ) {
    return getPath( this.$data, property );
  }

  /**
   *  Set the value of the given property.
   *
   *  @method set
   *  @param  property  The property name or dotted-property path {String};
   *  @param  newValue  The new value {Mixed};
   *
   *  @return An indication of whether or not the property was set {Boolean};
   */
  set( property, newValue ) {
    return setPath( this.$data, property, newValue );
  }

  /**
   *  Return a serializable JSON object representation of this instance.
   *
   *  @method toJSON
   *
   *  @return A serializable representation of this instance {Object};
   */
  toJSON() {
    return this.$data;
  }

  /**
   *  Update our internal state with new data.
   *
   *  @method merge
   *  @param  data    The new model data {Object};
   *
   *  @return `this` for a fluent interface {Model};
   */
  merge( data ) {
    this._deepMerge( data );

    return this;
  }

  /**
   *  Save the current model.
   *
   *  @method save
   *
   *  @return A promise for results {Promise};
   *          - on success, this model instance {Model};
   *          - on failure, an error {Error};
   */
  save() {
    return this.$collection.save( this );
  }

  /**
   *  Remove the current model from the collection.
   *
   *  @method remove
   *
   *  @return A promise for results {Promise};
   *          - on success, an indication of whether the model was deleted
   *          {Boolean};
   *          - on failure, an error {Error};
   */
  remove() {
    return this.$collection.remove( this );
  }

  /**
   *  Update the current model with the provided data and save.
   *
   *  @method update
   *  @param  data    The update data {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, this model instance {Model};
   *          - on failure, an error {Error};
   */
  update( data ) {
    const merged  = this._deepMerge( data );
    if ( merged !== true ) {
      return Promise.reject( merged );
    }

    return this.save();
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Update the current model with the provided data (no save).
   *
   *  @method _deepMerge
   *  @param  data    The update data {Object};
   *
   *  @return An indicatio of the merge status {Boolean};
   *  @protected
   */
  _deepMerge( data ) {
    if (! isObject(data)) {
      // Nothing to merge
      return true;
    }

    const sanitized = {...data};
    if (this._id && sanitized._id != null) {
      // Do NOT allow an override of the existing model _id.
      delete sanitized._id;
    }

    deepMerge( this.$data, sanitized );

    return true;
  }

  /* Protected methods }
   **************************************************************************/
}

/**
 *  The abstract base-class for a collection of models backed by a concrete
 *  data store.
 *
 *  @class  Collection
 *
 *  :NOTE: UUID references should be inserted as:
 *          DBRef( collection {String}, id {ObjectId}, [db {String}] ):
 *          {
 *            $db : '%db-name%',          // optional
 *            $ref: '%collectino-name%',
 *            $id : ObjectId('5126bc054aed4daf9e2ab772'),
 *          }
 *
 *          https://www.mongodb.com/docs/manual/reference/database-references/
 *          https://mongodb.github.io/node-mongodb-native/4.13/classes/DBRef.html
 */
class Collection {
  /**
   *  The Model class for instances within this collection.
   *
   *  @property $Model {Model};
   *  @static
   *  @readonly
   */
  static $Model   = Model;

  /**
   *  The name of this collection, should be plural of the name of the model.
   *  
   *  @property $name {String};
   *  @static
   *  @readonly
   */
  static $name    = 'models';

  /**
   *  The schema for Models within this collection
   *
   *  @property $schema {Object};
   *  @static
   *  @readonly
   */
  static get $schema()  { return this.$Model.$schema }

  /**
   *  Determine if the given object is a Model instance for this collection.
   *
   *  @method isModel
   *  @param  obj   The target object {Object};
   *
   *  @return true | false {Boolean};
   *  @static
   */
  static isModel( obj ) {
    return ( obj instanceof this.$Model );
  }

  // Proxies for static properties and methods
  get $Model()    { return this.constructor.$Model }
  get $schema()   { return this.constructor.$schema }
  get $name()     { return this.constructor.$Model.$name }

  /**
   *  Create a new instance
   *
   *  @constructor
   *  @param  ds    The concrete data store backing this collection {DataStore};
   */
  constructor( ds ) {
    if (! (ds instanceof DataStore )) {
      throw new Error('Missing ds {DataStore}');
    }

    Object.defineProperties( this, {
      /**
       *  The backing data store
       *
       *  @property $ds {DataStore};
       *  @readonly
       */
      $ds : { value: ds },
    });
  }

  /**
   *  Determine if the given object is a Model instance for this collection.
   *
   *  @method isModel
   *  @param  obj   The target object {Object};
   *
   *  @return true | false {Boolean};
   */
  isModel( obj )  { return this.constructor.isModel( obj ) }

  /**
   *  Create a new model instance for this collection
   *
   *  @method makeModel
   *  @param  data  The data to be used to instantiate the model {Object};
   *
   *  @return The new model instance {Model};
   */
  makeModel( data ) {
    return (new this.$Model( data, this ));
  }

  /**
   *  Generate an error and return a promise that rejects with that error.
   *
   *  @method rejectError
   *  @param  code    The error code {Number};
   *  @param  message The error message {String};
   *
   *  @return A Promse.reject() with a {code: , message:} {Object};
   */
  rejectError( code, message ) {
    const err = { code, message };

    return Promise.reject( err );
  }

  /**
   *  Find a single model using the given filter.
   *
   *  @method findOne
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, the model instance or null if not found
   *            {Model | null};
   *          - on failure, an error {Error};
   */
  async findOne( filter ) {
    const data  = await this.$ds.findOne( filter );
    const model = (data ? this.makeModel(data) : null);

    return model;
  }

  /**
   *  Save the given model.
   *
   *  @method save
   *  @param  model   The target model {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, this model instance {Model};
   *          - on failure, an error {Error};
   */
  async save( model ) {
    // assert( this.isModel( model ) );
    const res = this.$ds.save( model );

    // assert( res === model );

    return model;
  }

  /**
   *  Remove the given model from the collection.
   *
   *  @method remove
   *  @param  model   The target model {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, an indication of whether the model was deleted
   *          {Boolean};
   *          - on failure, an error {Error};
   */
  async remove( model ) {
    // assert( this.isModel( model ) );

    return this.$ds.remove( model );
  }

  /**
   *  Find one or more models matching the given filter.
   *
   *  @method find
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, an array of model instances, empty if none found
   *            {Array};
   *          - on failure, an error {Error};
   */
  async find( filter ) {
    const matches = await this.$ds.find( filter );
    const models  = matches.map( data => this.makeModel( data ) );

    return models;
  }

  /**
   *  Retrieve the number of entries in this collection.
   *
   *  @method count
   *
   *  @return A promise for results {Promise};
   *          - on success, the number of entries {Number};
   *          - on failure, an error {Error};
   */
  async count() {
    return this.$ds.count();
  }
}

module.exports  = { Model, Collection };
