const { Model, Collection } = require('../collection');

/**
 *  The log model
 *  @class    Log
 *  @extends  Model
 */
class Log extends Model {
  static $schema  = {
    bsonType: 'object',
    required: ['_id', 'created_by', 'created_on', 'storage', 'runs'],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of this log',
      },
      name          : {
        bsonType    : 'string',
        description : 'The name for this log type/class',
      },
      description   : {
        bsonType    : 'string',
        description : 'The description of this log type/class',
      },

      created_by    : {
        bsonType    : 'objectId',
        description : 'UUID of the creating user',
      },
      created_on    : {
        bsonType    : 'timestamp',
        description : 'The timestamp of creation',
      },
      updated_by    : {
        bsonType    : 'objectId',
        description : 'UUID of the user that last updated',
      },
      updated_on    : {
        bsonType    : 'timestamp',
        description : 'The timestamp of the last update',
      },

      /* Storage location(s) where raw log data associated with this type/class
       * will be stored for analysis.
       */
      storage       : {
        bsonType: 'object',
        required: ['s3_url'],
		    properties: {
          s3_url      : {
            bsonType    : 'string',
            description : 'The URL to an S3-compliant storage bucket.',
          },

          elastic_url : {
            bsonType    : 'string',
            description : 'The URL to an ElasticSearch index.',
          },
        },
      },

      /* :XXX: This is just a cache of
       *        this.db.$collection.runs.find({log_id: _id});
       */
      runs          : {
        bsonType    : 'array',
        description : "Runs performed against this log",
        items       : {
          bsonType    : 'objectId',
          description : 'UUID of a single run',
        },
      },
    },
  };
}

/**
 *  The logs collection
 *  @class    Logs
 *  @extends  Collection
 */
class Logs extends Collection {
  static $Model  = Log;
  static $name   = 'logs';
}

module.exports  = { Log, Logs };
