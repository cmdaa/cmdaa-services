const Crypto                = require('crypto');
const { Model, Collection } = require('../collection');

/**
 *  The user model
 *  @class    User
 *  @extends  Collection
 */
class User extends Model {
  static $schema  = {
    bsonType: 'object',
    required: ['_id', 'user_name', 'groups'],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'The UUID of this user',
      },
      user_name     : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's username",
      },
      password_hash : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The SHA256 has of the user's password",
      },
      email         : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's email address",
      },
      name_first    : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's first name",
      },
      name_last     : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's last name",
      },

      groups        : {
        bsonType    : 'array',
        //type        : 'array',
        description : "Groups to which this user belongs",
        items       : { type: "string" },
      },

      // Authentication information
      last_access   : {
        bsonType    : 'timestamp',
        description : "Last successful access (seconds since epoch)",
      },
      token_type    : {
        bsonType    : 'string',
        enum        : [ 'Bearer' ],
        //default     : 'Bearer',
        description : "The type of `access_token`",
      },
      access_token  : {
        bsonType    : 'string',
        description : "The current access token",
      },
      expires_in    : {
        bsonType    : 'int',
        description : "Expiration fo `access_token` seconds from `last_access`",
      },
    },
  };

  /**
   *  Update the authentication token for this user and return updated user
   *  information.
   *  @method authUpdate
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated model {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  async authUpdate() {
    // Create/refresh the user's authentication token
    const token = _generateToken();

    this.merge( {
      last_access: Date.now(),
      ...token,
    });

    return this.save();
  }
}

/**
 *  The users collection
 *  @class    Users
 *  @extends  Collection
 */
class Users extends Collection {
  static $Model  = User;
  static $name   = 'users';

  /**
   *  Update the authentication token for the given user and return updated
   *  user information.
   *  @method authUpdate
   *  @param  id    The ObjectId of the target user {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated user information {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  async authUpdate( id ) {
    const user  = await this.findOne( {_id:id} );

    // Was the user located?
    if (user == null) {
      return this.rejectError( 404, 'User not found');
    }

    return user.authUpdate();
  }

  /**
   *  Perform a user/password login.
   *  @method authPassword
   *  @param  auth                The incoming auth information {Object};
   *  @param  auth.username       The name of the target user {String};
   *  @param  auth.password_hash  The SHA256 hash of the the user's password
   *                              {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authorized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  async authPassword( auth ) {
    // Attempt authentication
    if (auth == null || auth.user_name == null && auth.password_hash == null) {
      return this.rejectError( 401, 'Unauthorized (invalid auth data)' );
    }

    // Lookup the target user and validate the password
    const filter  = { user_name: auth.user_name };
    const user    = await this.findOne( filter );

    console.log('%s.authPassword( %s ): user:',
                this.constructor.name, user);

    // Was the user located?
    if (user == null) {
      return this.rejectError( 401,
                                'Unauthorized (invalid auth data : user)');
    }

    // Validate the provided password hash
    if (auth.password_hash !== user.get('password_hash')) {
      return this.rejectError( 401,
                                'Unauthorized (invalid auth data : password)' );
    }

    return user;
  }

  /**
   *  Perform token-based authentication.
   *  @method authToken
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authorized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  async authToken( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this.rejectError( 401, 'Unauthorized (invalid auth data)' );
    }

    /* Attempt token-based authentication
     *    Bearer %token%
     *
     * See if we have a user with the given token
     */
    const filter  = { token_type  : type, access_token: token };
    const user    = await this.findOne( filter );

    // Was the user located?
    if (user == null) {
      return this.rejectError( 401,
                                'Unauthorized (invalid auth data : token)' );
    }

    // Ensure this token has not expired
    const last_access = user.get('last_access');
    const expires_in  = user.get('expires_in');
    if ( last_access == null || expires_in == null ) {
      // :XXX: Strange -- this user has no last access and/or expiration
      return this.rejectError( 500,
                      `Missing last_access/expires_in for user ${user._id}` );
    }

    const exp_secs  = last_access + expires_in;
    const expires   = new Date( exp_secs * 1000 );
    const now       = new Date();
    if ( now.getTime() >= expires.getTime() ) {
      return this.rejectError( 401,
                                'Unauthorized (token expired)' );
    }

    return user;
  }

  /**
   *  Refresh the identified access token.
   *  @method authRefresh
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  async authRefresh( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this.rejectError( 401, 'Unauthorized (invalid auth data)' );
    }

    /* Attempt to refresh the current access token
     *    Bearer %token%
     */
    const filter  = { token_type  : type, access_token: token };
    const user    = await this.findOne( filter );

    // Was the user located?
    if (user == null) {
      return this.rejectError( 401,
                                'Unauthorized (invalid auth data : token)' );
    }

    return user.authUpdate();
  }

  /**
   *  Revoke any existing access token.
   *  @method authRevoke
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with true {Boolean};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  async authRevoke( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this.rejectError( 401, 'Unauthorized (invalid auth data)' );
    }

    const filter  = { token_type  : type, access_token: token };
    const user    = await this.findOne( filter );

    // Was the user located?
    if (user == null) {
      return this.rejectError( 401,
                                'Unauthorized (invalid auth data : token)' );
    }

    return user.update({
      type        : type,
      token       : null,
      expires_in  : -1,
    });
  }

  /**
   *  Attempt to create/register a new user.
   *  @method registerUser
   *  @param  user                Information about the new user {Object};
   *  @param  user.user_name      The desired user name {String};
   *  @param  user.password_hash  The SHA-256 hash of the password {String};
   *  @param  [user.email]        The optional email address {String};
   *  @param  [user.name_first]   The optional first name {String};
   *  @param  [user.name_last]    The optional last name {String};
   *
   *  :NOTE:  If this is the first user, they will automatically be added to
   *          the 'admin' group. Otherwise, they will initial belong to no
   *          groups.
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with user {User};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  async registerUser( user ) {
    if (user == null || typeof(user) !== 'object') {
      return this.rejectError( 400, 'Missing user data' );
    }
    if (typeof(user.user_name) !== 'string' || user.user_name.length < 3) {
      return this.rejectError( 400, 'Missing/Invalid user.user_name' );
    }
    if (typeof(user.password_hash) !== 'string' ||
        user.password_hash.length < 3) {

      return this.rejectError( 400, 'Missing/Invalid user.password_hash' );
    }

    // Check to see if the requested `user_name` is available
    const filter    = {user_name: user.user_name};
    const existing  = await this.findOne( filter );
    if (existing) {
      return this.rejectError( 400, 'User exists' );
    }

    const count         = await this.count();
    const token         = _generateToken();
    const sanitizedData = {
      user_name     : user.user_name,
      password_hash : user.password_hash,
      groups        : [],

      last_access   : Date.now(),
      ...token,
    };

    if (user.email)       { sanitizedData.email      = user.email }
    if (user.name_first)  { sanitizedData.name_first = user.name_first }
    if (user.name_last)   { sanitizedData.name_last  = user.name_last }

    if (count < 1) {
      // Make this user an 'admin'
      sanitizedData.groups.push('admin');
    }

    const newUser = this.makeModel( sanitizedData );

    return newUser.save();
  }

}

/****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Generate an access token
 *  @method _gernate_token
 *
 *  @return New access token data {Object};
 *  @private
 */
function _generateToken() {
  const data  = {
    token_type  : 'Bearer',
    access_token: Crypto.randomUUID(),
    expires_in  : (24 * 60 * 60), // seconds
  };

  return data;
}

/* Private helpers }
 ****************************************************************************/

module.exports  = { Users };
