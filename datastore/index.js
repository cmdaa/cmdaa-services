/**
 *  An abstract adapter for a concrete data store backing a collection.
 *
 *  @class  DataStore
 */
class DataStore {
  /**
   *  Create a new instance
   *
   *  @constructor
   *  @param  db          The underlying concrete data store {Mixed};
   *  @param  Collection  The collection class being served {Collection};
   */
  constructor( db, Collection ) {
    Object.defineProperties( this, {
      /**
       *  The backing data store
       *
       *  @property $db {Mixed};
       *  @readonly
       */
      $db : { value: db },

      /**
       *  The collection class being served by this adapter.
       *
       *  @property $Collection {Collection};
       *  @readonly
       */
      $Collection : { value: Collection },
    });
  }

  /**
   *  Find a single model using the given filter.
   *
   *  @method findOne
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, the model instance or null if not found
   *            {Model | null};
   *          - on failure, an error {Error};
   */
  async findOne( filter ) {
    throw new Error('DataStore.findOne(): NYI');
  }

  /**
   *  Save the given model, inserting if it is not yet backed (! $isBacked()).
   *
   *  @method save
   *  @param  model   The target model data {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, this model instance {Model};
   *          - on failure, an error {Error};
   */
  async save( model ) {
    throw new Error('DataStore.save(): NYI');
  }

  /**
   *  Remove the given model from the collection.
   *
   *  @method remove
   *  @param  model   The target model {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, an indication of whether the model was deleted
   *          {Boolean};
   *          - on failure, an error {Error};
   */
  async remove( model ) {
    throw new Error('DataStore.remove(): NYI');
  }

  /**
   *  Find one or more models matching the given filter.
   *
   *  @method find
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, an array of model instances, empty if none found
   *            {Array};
   *          - on failure, an error {Error};
   */
  async find( filter ) {
    throw new Error('DataStore.find(): NYI');
  }

  /**
   *  Retrieve the number of entries in this collection.
   *
   *  @method count
   *
   *  @return A promise for results {Promise};
   *          - on success, the number of entries {Number};
   *          - on failure, an error {Error};
   */
  async count() {
    throw new Error('DataStore.count(): NYI');
  }
}

module.exports  = { DataStore };
