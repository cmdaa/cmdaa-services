const { MongoDataStore,
        User, Users }         = require('..');
const { MockMongo,
        MockMongoCollection } = require('./mock-mongo');

const mongo = new MockMongo();
const mds   = new MongoDataStore( mongo, Users );
console.log('mds:', mds);

const users = new Users( mds );

run_tests( users );

/****************************************************************************/
async function run_tests( users ) {
  let res, model;

  model = await test_register( users );
  console.log('registerUser()    :', model);
  console.log('model._id         :', (model && model._id) );
  console.log('model.toJSON()    :', (model && model.toJSON()) );

  model = users.makeModel( {user_name:'user1'} );
  console.log('makeModel         :', model);

  console.log('model._id         :', (model && model._id) );
  console.log('model.toJSON()    :', (model && model.toJSON()) );

  res = await test_save( model );
  console.log('model.save()      :', res);
  console.log('res._id           :', (res && res._id) );
  console.log('res.toJSON()      :', (res && res.toJSON()) );

  res = await test_authUpdate( model );
  console.log('model.authUpdate():', res);
  console.log('res._id           :', (res && res._id) );
  console.log('res.toJSON()      :', (res && res.toJSON()) );

  res = await test_find( users );
  console.log('users.find()      : %s models', res.length);
  res.forEach( (model,idex) => {
    console.log('  %s:', idex, model.toJSON());
  });
}

async function test_register( users ) {
  const data  = {user_name:'dep', password_hash:'abc'};
  let   model;

  try {
    model = await users.registerUser( data );

    //console.log('>>> registerUser:', model.toJSON());

  } catch( err ) {
    console.error('*** registerUser:', err);

  }

  return model;
}

async function test_save( model ) {
  let res;

  try {
    res = await model.save();

    //console.log('>>> model.save:', res);

  } catch( err ) {
    console.error('*** model.save:', err);

  }

  return res;
}

async function test_authUpdate( model ) {
  let res;

  try {
    res = await model.authUpdate();

    //console.log('>>> model.authUpdate:', res);

  } catch( err ) {
    console.error('*** model.authUpdate:', err);
  }

  return res;
}

async function test_find( users ) {
  let res;

  try {
    res = await users.find();

  } catch( err ) {
    console.error('*** users.find:', err);
  }

  return res;
}
