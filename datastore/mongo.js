const DataStore   = require('./').DataStore;
const ObjectId    = require('mongodb').ObjectId;
const Timestamp   = require('mongodb').Timestamp;

/**
 *  A concrete adapater using MongoDb as the backing store.
 *
 *  @class  MongoDataStore
 */
class MongoDataStore extends DataStore {
  /**
   *  Create a new instance
   *
   *  @constructor
   *  @param  db          A connection to the underlying Mongo database
   *                      {Mongo};
   *  @param  Collection  The collection class being served {Collection};
   */
  constructor( db, Collection ) {
    super( db, Collection );

    Object.defineProperties( this, {
      /**
       *  The underlying mongo collection
       *
       *  @property $mongoCollection {MongoCollection};
       *  @readonly
       */
      $mongoCollection  : { value: null, writable: true },

      /**
       *  An indication of ready state, true after successful attach.
       *  
       *  @property $isReady {Boolean};
       *  @readonly
       */
      $isReady  : { value: null, writable: true },

      /**
       *  An promise for attachment.
       *  
       *  @property $ready {Promse};
       *  @readonly
       */
      $ready    : { value: null, writable: true },
    });

    this.$ready = this._attach()
      .then( res => {
        this.$isReady = true;
      })
      .catch( err => {
        console.error('*** %s(): failed attach:',
                      this.constructor.name, err);
      });
  }

  /**
   *  Find a single model using the given filter.
   *
   *  @method findOne
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, the model instance or null if not found
   *            {Model | null};
   *          - on failure, an error {Error};
   */
  async findOne( filter ) {
    if (! this.$isReady) { await this.$ready }

    const mongoFilter = this._toMongo( filter );

    return this.$mongoCollection.findOne( mongoFilter )
            .then( data => {
              const jsonData  = this._fromMongo( data );
              return jsonData;
            });
  }

  /**
   *  Save the given model.
   *
   *  @method save
   *  @param  model   The target model data {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, this (updated) model instance {Model};
   *          - on failure, an error {Error};
   */
  async save( model ) {
    if (! this.$isReady) { await this.$ready }

    /*
    console.log('%s.save(): $isBacked[ %s ], _id[ %s ]:',
                this.constructor.name,
                String( model.$isBacked() ), model._id, model.toJSON());
    // */

    const mongoData = this._toMongo( model.toJSON() );
    let   oid       = mongoData._id;
    if (! model.$isBacked()) {
      // Insert a new model
      // assert( oid == null )
      const res = await this.$mongoCollection.insertOne( mongoData );

      /*
      console.log('%s.save(): res:', this.constructor.name, res);
      // */

      // { acknowledged: insertedId: }
      if (res.insertedId == null) {
        throw new Error('No insert');
      }

      oid = res.insertedId;

    } else {
      // Update an existing model
      // assert( oid instanceof ObjectId );
      const mongoUpdate = { $set: mongoData };

      /*
      console.log('%s.save(): Existing model, oid:',
                  this.constructor.name, oid);
      // */

      await this.$mongoCollection.updateOne( {_id: oid}, mongoUpdate);
      // { nMatched:, nUpserted:, nModified: }
    }

    /*
    console.log('%s.save(): find[ %s ] ...',
                  this.constructor.name, oid);
    // */

    // Retrieve the changes
    const data  = await this.$mongoCollection.findOne( {_id: oid} );
    if (data == null) {
      console.error('*** %s.save(): Cannot find model', this.constructor.name);

      throw new Error('Failed to find the model we just updated');
    }

    /*
    console.log('%s.save(): find[ %s ]:',
                  this.constructor.name, oid, data);
    // */

    const jsonData  = this._fromMongo( data );

    return model.merge( jsonData );
  }

  /**
   *  Remove the given model from the collection.
   *
   *  @method remove
   *  @param  model   The target model {Model};
   *
   *  @return A promise for results {Promise};
   *          - on success, an indication of whether the model was deleted
   *          {Boolean};
   *          - on failure, an error {Error};
   */
  async remove( model ) {
    if (! this.$isReady) { await this.$ready }

    if ( ! model.$isBacked()) {
      // Not backed, nothing to do
      return true;
    }

    // assert( model._id != null );
    const mongoFilter = { _id: this._toObjectId( model._id ) };

    // assert( mongoFilter._id instanceof ObjectId );
    const res = await this.$mongoCollection.deleteOne( mongoFilter );
    // { acknowledged:, deletedCount: }
    return (res.deletedCount === 1);
  }

  /**
   *  Find one or more models matching the given filter.
   *
   *  @method find
   *  @param  filter  The filter to be used to locate the target model {Object};
   *
   *  @return A promise for results {Promise};
   *          - on success, an array of model data, empty if none found
   *            {Array};
   *          - on failure, an error {Error};
   */
  async find( filter ) {
    if (! this.$isReady) { await this.$ready }

    const mongoFilter = this._toMongo( filter );
    const cursor      = await this.$mongoCollection.find( mongoFilter );
    const models      = []

    await cursor.forEach( data => {
      const jsonData  = this._fromMongo( data );
      models.push( jsonData );
    });

    console.log('%s.find(): models:', this.constructor.name, models);

    return models;
  }

  /**
   *  Retrieve the number of entries in this collection.
   *
   *  @method count
   *
   *  @return A promise for results {Promise};
   *          - on success, the number of entries {Number};
   *          - on failure, an error {Error};
   */
  async count() {
    return this.$mongoCollection.count();
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Does the given collection exist?
   *  @method _collectionExists
   *  @param  name    The target collection {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the collection {Collection};
   *          - on failure, rejected with an error {Error};
   */
  _collectionExists( name ) {
    return this.$db.listCollections( {name} ).next();
  }

  /**
   *  Attach to the collection, ensuring it exists.
   *  @method _attach
   *
   *  @return A promise for results {Promise};
   *          - on success, true {Boolean};
   *          - on failure, an error {Error};
   *  @protected
   */
  async _attach() {
    const $name = this.$Collection.$name;

    /*
    console.log('>>> %s attach to collection [ %s ] ...',
                  this.constructor.name, $name);
    // */

    const exists  = await this._collectionExists( $name );
    if (exists) {
      /*
      console.log('>>> %s collection [ %s ] exists:',
                  this.constructor.name, $name, exists);
      // */

      this.$mongoCollection = this.$db.collection( $name );

      return true;
    }

    this.$mongoCollection = await this._createCollection();

    /*
    console.log('>>> %s created:',
                this.constructor.name, this.$mongoCollection);
    // */

    return true;
  }

  /**
   *  Create the MongoDb collection.
   *
   *  @method _createCollection
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the collection {MongoCollection};
   *          - on failure, rejected with an error {Error};
   *  @protected
   */
  async _createCollection() {
    const $name   = this.$Collection.$name;
    const $schema = this.$Collection.$schema;

    /*
    console.log('>>> %s create collection [ %s ] ...',
                this.constructor.name, $name);
    // */

    const opts  = {};
    if ($schema) {
      opts.validator = { $jsonSchema: $schema };
    }

    let $col;
    try {
      $col = await this.$db.createCollection( $name, opts );

      /*
      console.log('>>> %s created collection [ %s ]:',
                  this.constructor.name, $name, $col);
      // */

    } catch(err) {
      const msg = `failed to create collection [ ${$name} ]: `
                +     err.message;

      // /*
      console.log('*** %s %s',
                this.constructor.name, msg);
      // */

      throw new Error( msg );
    }

    return $col;
  }

  /**
   *  Given an incoming object, convert any fields that need special types.
   *
   *  @method _toMongo
   *  @param  obj   The incoming object {Object};
   *
   *  @return The updated `obj` {Object};
   *  @protected
   */
  _toMongo( obj ) {
    if (obj == null) { return obj }

    const mongoObj  = {...obj};
    const $schema   = this.$Collection.$schema;

    if ($schema == null || $schema.properties == null)  {
      return mongoObj;
    }

    Object.entries( obj ).forEach( ([key,val]) => {
      const $def  = $schema.properties[key];
      if ($def == null) { return }

      switch( $def.bsonType ) {
        case 'objectId': {
          const oid = this._toObjectId( val );

          if (! (oid instanceof ObjectId)) {
            // Invalid value
            throw new Error(`invalid value for '${key}' `
                            + '(cannot be converted to ObjectId)' );
          }

          mongoObj[ key ] = oid;

        } break;

        case 'timestamp': {
          const ts  = this._toTimestamp( val );

          if (! (ts instanceof Timestamp)) {
            // Invalid value
            throw new Error(`invalid value for '${key}' `
                            + '(cannot be converted to Timestamp)' );
          }

          mongoObj[ key ] = ts;

        } break;
      }
    });

    return mongoObj;
  }

  /**
   *  Attempt to convert the given value to an ObjectId.
   *
   *  @method _toObjectId
   *  @param  val     The target value {Mixed};
   *
   *  @return The ObjectId {ObjectId};
   *          OR throw an error if conversion is not possible {Error};
   *  @protected
   */
  _toObjectId( val ) {
    const isObjectId  = (val instanceof ObjectId);
    let   oid         = ( isObjectId && val );

    if (! isObjectId) {
      // Attempt to create an ObjectId from `val`
      try {
        oid = new ObjectId( val );

      } catch(ex) {
        console.error('toObjectId( %s ):', val, ex);
      }
    }

    return oid;
  }

  /**
   *  Attempt to convert the given value to a Timestamp.
   *
   *  @method _toTimestamp
   *  @param  val     The target value {Mixed};
   *
   *  @return The Timestamp {Timestamp};
   *          OR throw an error if conversion is not possible {Error};
   *  @protected
   */
  _toTimestamp( val ) {
    let ts  = val;
    if (val instanceof Timestamp) { return ts }

    let date  = val;
    if (typeof(val) === 'string') {
      // Attempt to parse this as a Date
      date = new Date( val );

    } else if (typeof(val) === 'number') {
      // ASSUME this is a Javascript timestamp (ms since epoch)
      date = new Date( val );
    }

    if (date instanceof Date) {
      const now_s = Math.floor( date.getTime() / 1000 );
      console.log('>>> %s._toTimestamp( %s ): date[ %s => %s ] ...',
                  this.constructor.name, val, date, now_s);

      try {
        ts = new Timestamp( {i:0, t:now_s} );

      } catch( err ) {
        console.error('*** %s._toTimestamp( %s ): '
                      + 'Cannot convert date[ %s ] to Timestamp:',
                    this.constructor.name, val, date, err);
      }
    }

    return ts;
  }

  /**
   *  Given an incoming Mongo object (_toMongo()), convert any fields that need
   *  special types.
   *
   *  @method _fromMongo
   *  @param  mongoObj  The incoming mongo object {Object};
   *
   *  @return The updated `mongoObj` {Object};
   *  @protected
   */
  _fromMongo( mongoObj ) {
    if (mongoObj == null) { return mongoObj }

    /*
    console.log('>>> %s._fromMongon(): mongoObj:',
                this.constructor.name, mongoObj);
    // */

    const obj     = {...mongoObj};
    const $schema = this.$Collection.$schema;

    if ($schema == null || $schema.properties == null)  {
      return obj;
    }

    Object.entries( mongoObj ).forEach( ([key,val]) => {
      const $def  = $schema.properties[key];
      if ($def == null) { return }

      switch( $def.bsonType ) {
        case 'objectId': {
          const str = this._fromObjectId( val );

          if (typeof(str) !== 'string') {
            // Invalid value
            throw new Error(`invalid value for '${key}' `
                            + '(cannot be converted to String)' );
          }

          obj[ key ] = str;

        } break;

        case 'timestamp': {
          const ts  = this._fromTimestamp( val );

          if (typeof(ts) !== 'number') {
            // Invalid value
            throw new Error(`invalid value for '${key}' `
                            + '(cannot be converted to Javascript timestamp)' );
          }

          obj[ key ] = ts;

        } break;
      }
    });

    return obj;
  }

  /**
   *  Attempt to convert the given ObjectId to a string.
   *
   *  @method _fromObjectId
   *  @param  oid   The target value {ObjectId};
   *
   *  @return The string version of `val` {String};
   *          OR throw an error if conversion is not possible {Error};
   *  @protected
   */
  _fromObjectId( oid ) {
    return String( oid );
  }

  /**
   *  Attempt to convert the given Timestamp to a Javascript timestamp (ms
   *  since epoch);
   *
   *  @method _fromTimestamp
   *  @param  ts    The target value {Timestamp};
   *
   *  @return The Javascript timestamp (ms since epoch) {Number};
   *          OR throw an error if conversion is not possible {Error};
   */
  _fromTimestamp( ts ) {
    let res;

    if (ts && typeof(ts.getHighBits) === 'function') {
      res = ts.getHighBits();

    } else if (ts && typeof(ts.high) === 'number') {
      res = ts.high;

    } else {
      console.error('*** %s._fromTimestamp(): ts is NOT a Timestamp instance:',
                    this.constructor.name, ts);
    }

    return res;
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = { MongoDataStore };
