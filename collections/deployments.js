const { Model, Collection } = require('../collection');

/**
 *  The deployment model
 *  @class    Deployment
 *  @extends  Model
 */
class Deployment extends Model {
  static $schema  = {
    bsonType: 'object',
    required: [
      '_id', 'run_id',
      'created_by', 'created_on',

      'log_paths', 'status', 'groks',
    ],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'The UUID of this deployment',
      },
      run_id        : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the run used for this deployment',
      },

      created_by    : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the submitting user',
      },
      created_on    : {
        bsonType    : 'timestamp',
        description : 'Submission time (seconds since epoch)',
      },

      updated_by    : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of user that last updated this deployment',
      },
      updated_on    : {
        bsonType    : 'timestamp',
        description : 'Last update time (seconds since epoch)',
      },

      started_by    : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the user that last started this deployment',
      },
      started_on    : {
        bsonType    : 'timestamp',
        description : 'Last start time (seconds since epoch)',
      },

      stopped_by    : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the user that last stopped this deployment',
      },
      stopped_on    : {
        bsonType    : 'timestamp',
        description : 'Last stop time (seconds since epoch)',
      },

      /*****************************************************
       * Kubernetes (k8s) daemonset deployed fluentd
       *
       */
      k8s : {
        bsonType    : 'object',
        description : 'Kubernetes (k8s) daemonset deployed fluentd',
        required: [
          'labels', 'prefix', 'log_path',
        ],
        properties: {
          labels  : {
            bsonType    : 'array',
            //type        : 'array',
            description : 'k8s node labels identifying target nodes',
            items       : { type: 'string' },
          },
          prefix  : {
            bsonType    : 'string',
            description : 'The prefix to use for associated k8s resources',
          },

          log_path  : {
            bsonType    : 'string',
            description : 'The (glob) path to the log data on target',
          },
        },
      },

      /*****************************************************
       * Ansible deployed fluentd
       *
       */
      ansible    : {
        bsonType    : 'object',
        description : 'ssh deployed fluentd',
        required: [
          'nodes', 'log_path', 'ssh_key',
        ],
        properties: {
          nodes   : {
            bsonType    : 'array',
            //type        : 'array',
            description : 'The set of bare-metal target nodes',
            items       : { type: 'string' },
          },
          log_path: {
            bsonType    : 'string',
            description : 'The (glob) path to the log data on target',
          },
          ssh_key : {
            bsonType    : 'object',
            description : 'ssh key for access to the bare-metal nodes',
            required: [ 'private' ],
            properties: {
              'public'  : {
                bsonType    : 'string',
                description : 'The ssh public key',
              },
              'private' : {
                bsonType    : 'string',
                description : 'The ssh private key',
              },
            },
          },
        },
      },

      status        : {
        bsonType    : 'object',
        description : 'Deployment status',
        required    : [
          'isRunning', 'message',
        ],
		    properties  : {
          isRunning     : {
            bsonType    : 'bool',
            description : 'Is this deployment active (running)',
          },
          message       : {
            bsonType    : 'string',
            description : 'Deployment status message',
          },
        },
      },

      groks         : {
        bsonType    : 'array',
        description : 'The set of groks used for this deployment',
        items       : {
          bsonType    : 'objectId',
          description : 'UUID of a grok',
        },
      },
    },
  };
}

/**
 *  The deployments collection
 *  @class    Deployments
 *  @extends  Collection
 */
class Deployments extends Collection {
  static $Model  = Deployment;
  static $name   = 'deployments';
}

module.exports  = { Deployment, Deployments };
