const { Model, Collection } = require('./collection');
const { DataStore }         = require('./datastore');
const { MongoDataStore }    = require('./datastore/mongo');

const { Deployment, Deployments } = require('./collections/deployments');
const { Grok, Groks }             = require('./collections/groks');
const { Log, Logs }               = require('./collections/logs');
const { Run, Runs }               = require('./collections/runs');
const { User, Users }             = require('./collections/users');

module.exports = {
  Model,      Collection,
  DataStore,  MongoDataStore,

  Deployment, Deployments,
  Grok,       Groks,
  Log,        Logs,
  Run,        Runs,
  User,       Users,

  // Groups
  Collections : { Deployments, Groks, Logs, Runs, Users },
  Models      : { Deployment, Grok, Log, Run, User },
};
