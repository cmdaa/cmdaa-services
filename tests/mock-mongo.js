const ObjectId      = require('mongodb').ObjectId;
const { deepMerge } = require('../deepMerge');

/**
 *  Provide a simple, memory-based mock of Mongo.
 *
 */
class MockMongo {
  constructor() {
    this.$collections = new Map();
  }

  collectionExists( name )  {
    return this.$collections.has( name );
  }

  collection( name )  {
    return this.$collections.get( name );
  }

  createCollection( name ) {
    if (this.collectionExists(name)) {
      return this.collection(name);
    }

    const col = new MockMongoCollection( this );

    this.$collections.set( name, col );

    return col;
  }
}

class MockMongoCollection {
  constructor( mongo ) {
    this.mongo  = mongo;
    this.$data  = new Map();
  }

  async findOne( filter ) {
    const oid   = filter._id;
    let   res;

    if (oid) {
      res = this.$data.get( String(oid) );

    } else {
      // Search
      const filterEntries = Object.entries(filter);

      this.$data.forEach( (oid, data) => {
        if (res) { return }

        const match = filterEntries.every( ([prop,val]) => {
          return (data[prop] == val);
        });

        if (match) {
          res = data;
        }
      });

      return res;
    }

    /*
    console.log('%s.findOne( %s ): oid[ %s ], res:',
                this.constructor.name, JSON.stringify(filter), oid, res);
    // */

    return res;
  }

  async insertOne( data ) {
    const oid = new ObjectId();

    data._id = oid;

    /*
    console.log('%s.insertOne(): oid[ %s ], data:',
                this.constructor.name, oid, data);
    // */

    this.$data.set( String(oid), data );

    return { acknowleded: true, insertedId: oid };
  }

  async updateOne( filter, update ) {
    const oid   = filter._id;
    const data  = this.$data.get( String(oid) );
    const res   = { nMatched: 0, nUpserted: 0, nModified: 0 };

    if (data != null) {
      const updateSet = (update.$set || {});
      delete updateSet._id;

      deepMerge( data, updateSet );

      // :XXX: Probably not necessary since deepMerge()( is in-place
      this.$data.set( String(oid), data );

      res.nMatched  = 1;
      res.nModified = 1;

    } else {
      console.error('*** %s.updateOne(): filter[ %s ]: NOT FOUND, oid:',
                    this.constructor.name, JSON.stringify(filter), oid);
    }

    return res;
  }

  async deleteOne( filter ) {
    const oid   = filter._id;
    const res   = { acknowledged: true, deletedCount: 0 };

    console.log('%s.deleteOne(): oid:', this.constructor.name, oid);

    if (this.$data.has( oid )) {
      this.$data.delete( oid );
      res.deletedCount = 1;
    }

    return res;
  }

  async find( filter ) {
    const cursor  = {
      data: [],
      toArray() { return this.data },
    };

    if (filter == null || typeof(filter) !== 'object') {
      cursor.data = Array.from( this.$data.values() );
      return cursor;
    }

    const filterEntries = Object.entries(filter);

    this.$data.forEach( (oid, data) => {
      const match = filterEntries.every( ([prop,val]) => {
        return (data[prop] == val);
      });

      if (match) {
        cursor.data.push( data );
      }
    });

    return cursor;
  }

  async count() {
    return this.$data.size;
  }
}

module.exports = {
  MockMongo,
  MockMongoCollection,
}
