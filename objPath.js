/**
 *  Retrieve the value of the provided property name or dotted-property path.
 *
 *  @method getPath
 *  @param  obj       The object from which to retrieve the value {Object};
 *  @param  property  The property name or dotted-property path {String};
 *
 *  @return The property value {Mixed};
 */
function getPath( obj, property ) {
  if (typeof(property) !== 'string') {
    return
  }

  const path  = property.split('.');
  let   val   = obj;

  path.some( name => {
    if (val == null)  { return true /* terminate iteration */ }
    val = val[name];

    return false; // continue iteration
  });

  return val;
}

/**
 *  Set the value of the provided property name or dotted-property path.
 *
 *  @method setPath
 *  @param  obj       The object in which to set the new value {Object};
 *  @param  property  The property name or dotted-property path {String};
 *  @param  newValue  The new value {Mixed};
 *
 *  @return An indication of whether or not the property was set {Boolean};
 */
function setPath( obj, property, newValue ) {
  if (typeof(property) !== 'string') {
    return false;
  }

  const path  = property.split('.');
  const last  = path.pop();
  const reInt = /^[0-9]+$/;
  let   val   = obj;

  path.some( name => {
    if (val == null)  { return true /* terminate iteration */ }

    if (val[name] == null) {
      if (name.match( reInt )) {
        // Array
        val[name] = [];
      } else {
        // Object
        val[name] = {};
      }
    }

    val = val[name];

    return false; // continue iteration
  });

  if (val != null) {
    if (val[last] === newValue) { return false }

    val[last] = newValue;
  }

  return true;
}

module.exports  = {
  getPath,
  setPath,
};
